package com.IRA.qa.testcases;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.IRA.qa.base.TestBase;
import com.IRA.qa.pages.LoginPage;
import com.IRA.qa.util.WaitHelper;

public class LoginPageTest extends TestBase{
	LoginPage loginPage;
	WaitHelper waithelper;
	//TestBase testbase;
	
	
	/*
	 * public LoginPageTest(){ super(); }
	 */
	
	@BeforeSuite
	public void setUp(){
		initialization();
		loginPage = new LoginPage();	
		waithelper = new WaitHelper(driver);
	}
	
	
	  @Test(priority=1) public void loginTest()
	  
	  { 
		  String username =	  prop.getProperty("username"); 
		  String password = prop.getProperty("password");
	  System.out.println(username); 
	  System.out.println(password);
	  
	  loginPage.login(username,password );
	  
	  }
	 
	
	
	@Test(priority=2)
	
	public void loginPageTitleTest(){
		/*
		 * String username = prop.getProperty("username"); String password =
		 * prop.getProperty("password"); System.out.println(username);
		 * System.out.println(password);
		 * 
		 * loginPage.login(username,password );
		 */
		
WebElement Profile_Display = driver.findElement(By.xpath(Locatorproperties.getProperty("Profile_Display")));
		WaitHelper.waitForElement(driver,10000,Profile_Display);
		/*
		 * WebElement Login_Title =
		 * driver.findElement(By.xpath(Locatorproperties.getProperty("Profile_Display"))
		 * ); WaitHelper.waitForElement(driver,10000,Login_Title);
		 */
	String title = Profile_Display.getText();
		//String title = loginPage.validateLoginPageTitle();
		System.out.println(title);
		try {
			Assert.assertEquals(title, "K");
            
        } catch (Exception e) {
            Assert.fail();

        }
		
	}
	

	
	
	
	
	@AfterSuite
	public void tearDown(){
		//driver.quit();
	}
	
	
	
	

}
