package com.IRA.graphql.base;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.IRA.qa.base.Testbasewithoutestname;
import com.IRA.qa.util.Dataprovider;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import okhttp3.Response;

public class CreateAsessment extends Testbasewithoutestname {
String[] Assessmentid = new String[5];
	
	public String ColName = "Result";
	public int col_num;
	public int No = 1;
	
	@Test(dataProvider = "Create Assessments", dataProviderClass = Dataprovider.class, priority = 1)
	public void MutationforCreateAssessment(String Num, String data1, String Assessmentname, String data2,String Assessmentdescription, String data3,String Assessmenttype,String data4,String Query) throws IOException {
		/*
		 * System.out.println(Num); System.out.println(data1);
		 * System.out.println(Assessmentname); System.out.println(data2);
		 * System.out.println(Assessmentdescription); System.out.println(data3);
		 * System.out.println(Assessmenttype); System.out.println(data4);
		 */
		System.out.println(Query);


		String graphqlPayload_raw = data1 + Assessmentname + data2+Assessmentdescription+data3+Assessmenttype+data4;
		InputStream targetStream = new ByteArrayInputStream(Query.getBytes());
		String graphqlPayload = GraphqlTemplate.parseGraphqlwithoutvariables(targetStream);
		System.out.println(graphqlPayload);
		String AccessToken = MutationforToken();
		Response response = prepareResponse(graphqlPayload, AccessToken);
		long tx = response.sentRequestAtMillis();
		long rx = response.receivedResponseAtMillis();
		System.out.println("response time : " + (rx - tx) + " ms");
		String jsonData_MutationforCreateAssessment = response.body().string();
		System.out.println(jsonData_MutationforCreateAssessment);
		Reporter.log(jsonData_MutationforCreateAssessment);
		JsonNode jsonNode = new ObjectMapper().readTree(jsonData_MutationforCreateAssessment);
		Assessmentid[No-1] = jsonNode.get("data").get("createAssessment").get("id").asText();
		System.out.println("qid is");

		System.out.println(Assessmentid[No - 1]);
		try {
			  File DataSource = new File("C:\\Users\\KishoreMurkhanad\\git\\repository\\IRA-test\\src\\test\\resources\\reuse\\Assessmentids.xlsx");
			FileInputStream file_input_stream = new FileInputStream(DataSource);
			Workbook wb = new XSSFWorkbook(file_input_stream);
			Sheet sh = wb.getSheet("Assids");

			// ExcelData = new String[101][2];

			
					String Qid = Assessmentid[No - 1];
					System.out.println("This Assid");

					System.out.println(Qid);
					Cell QidNocell = sh.getRow(No).getCell(0);
					Cell Qidcell = sh.getRow(No).getCell(1);
					if(QidNocell == null){
						QidNocell = sh.getRow(No).createCell(0);
						}
					QidNocell.setCellValue(Num);
					if(Qidcell == null){
						Qidcell = sh.getRow(No).createCell(1);
						}
					Qidcell.setCellValue(Qid);
					// System.out.println(Arrays.deepToString(arrayExcelData));
					File DataSource1 = new File("C:\\Users\\KishoreMurkhanad\\git\\repository\\IRA-test\\src\\test\\resources\\reuse\\Assessmentids.xlsx");
					FileOutputStream file_output_stream = new FileOutputStream(DataSource1);
			        wb.write(file_output_stream);
					file_output_stream.close();
					if (col_num == -1) {
						System.out.println("Column you are searching for does not exist");
					}

		}

		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		No =No+1;
		System.out.println(No);
	}
	}

