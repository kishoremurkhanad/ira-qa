package com.IRA.graphql.base;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import com.IRA.qa.base.Testbasewithoutestname;
import com.IRA.qa.util.Dataprovider;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import okhttp3.Response;

public class CreateQuestions extends Testbasewithoutestname {
	String[] Questionid = new String[100];
	
	public String ColName = "Result";
	public int col_num;
	public int No = 1;

	/*
	 * @Test(priority = 1) public void MutationforcreateQuestion() throws
	 * IOException {
	 * 
	 * File file = new
	 * File("src/test/resources/reuse/MutationforcreateQuestion.graphql");
	 * 
	 * ObjectMapper mapper = new ObjectMapper(); ObjectNode root =
	 * mapper.createObjectNode();
	 * 
	 * System.out.println(id); if (id.contains("null")) {
	 * System.out.println("id is null");
	 * 
	 * } root.put("id", id);
	 * 
	 * String graphqlPayload = GraphqlTemplate.parseGraphqlwithvariables(file,
	 * null); System.out.println(graphqlPayload); String AccessToken =
	 * MutationforToken(); Response response = prepareResponse(graphqlPayload,
	 * AccessToken); long tx = response.sentRequestAtMillis(); long rx =
	 * response.receivedResponseAtMillis();
	 * System.out.println("response time : "+(rx - tx)+" ms"); String
	 * jsonData_MutationforcreateQuestion = response.body().string();
	 * System.out.println(jsonData_MutationforcreateQuestion);
	 * Reporter.log(jsonData_MutationforcreateQuestion); JsonNode jsonNode = new
	 * ObjectMapper().readTree(jsonData_MutationforcreateQuestion); Questionid[0] =
	 * jsonNode.get("data").get("createQuestion").get("id").asText(); }
	 */

	@Test(dataProvider = "Add Questions", dataProviderClass = Dataprovider.class, priority = 1)
	public void MutationforCreateQuestion(String Num, String data1, String Questionname, String data2,
			String Competencyid, String data3, String questiontype, String questiontype_answer) throws IOException {

		String graphqlPayload_raw = data1 + Questionname + data2 + Competencyid + data3 + questiontype
				+ questiontype_answer;
		InputStream targetStream = new ByteArrayInputStream(graphqlPayload_raw.getBytes());
		String graphqlPayload = GraphqlTemplate.parseGraphqlwithoutvariables(targetStream);
		System.out.println(graphqlPayload);
		String AccessToken = MutationforToken();
		Response response = prepareResponse(graphqlPayload, AccessToken);
		long tx = response.sentRequestAtMillis();
		long rx = response.receivedResponseAtMillis();
		System.out.println("response time : " + (rx - tx) + " ms");
		String jsonData_MutationforCreateAssessment = response.body().string();
		System.out.println(jsonData_MutationforCreateAssessment);
		Reporter.log(jsonData_MutationforCreateAssessment);
		JsonNode jsonNode = new ObjectMapper().readTree(jsonData_MutationforCreateAssessment);
		Questionid[No - 1] = jsonNode.get("data").get("createQuestion").get("id").asText();
		System.out.println("qid is");

		System.out.println(Questionid[No - 1]);
		try {
			  File DataSource = new File("C:\\Users\\KishoreMurkhanad\\git\\repository\\IRA-test\\src\\test\\resources\\reuse\\Testing.xlsx");
			FileInputStream file_input_stream = new FileInputStream(DataSource);
			Workbook wb = new XSSFWorkbook(file_input_stream);
			Sheet sh = wb.getSheet("Qids");

			// ExcelData = new String[101][2];

			
					String Qid = Questionid[No - 1];
					System.out.println("This Qid");

					System.out.println(Qid);
					Cell QidNocell = sh.getRow(No).getCell(0);
					Cell Qidcell = sh.getRow(No).getCell(1);
					if(QidNocell == null){
						QidNocell = sh.getRow(No).createCell(0);
						}
					QidNocell.setCellValue(Num);
					if(Qidcell == null){
						Qidcell = sh.getRow(No).createCell(1);
						}
					Qidcell.setCellValue(Qid);
					// System.out.println(Arrays.deepToString(arrayExcelData));
					File DataSource1 = new File("C:\\Users\\KishoreMurkhanad\\git\\repository\\IRA-test\\src\\test\\resources\\reuse\\Testing.xlsx");
					FileOutputStream file_output_stream = new FileOutputStream(DataSource1);
			        wb.write(file_output_stream);
					file_output_stream.close();
					if (col_num == -1) {
						System.out.println("Column you are searching for does not exist");
					}

		}

		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		No =No+1;
		System.out.println(No);
	}

	

	/*
	 * @AfterClass public void updatequestionids() throws IOException {
	 * 
	 * // String[][] ExcelData = null;
	 * 
	 * try { File DataSource = new File(
	 * "C:\\Users\\KishoreMurkhanad\\git\\repository\\IRA-test\\src\\test\\resources\\reuse\\CreateQuestion_ans.xlsx"
	 * ); FileInputStream file_input_stream = new FileInputStream(DataSource);
	 * Workbook wb = new XSSFWorkbook(file_input_stream); Sheet sh =
	 * wb.getSheet("Qids");
	 * 
	 * // ExcelData = new String[101][2];
	 * 
	 * for (int i = 1; i < 2; i++) {
	 * 
	 * for (int j = 0; j < 1; j++) { String Qid = Questionid[i - 1];
	 * sh.getRow(i).getCell(j).setCellValue(Qid.toString()); //
	 * System.out.println(Arrays.deepToString(arrayExcelData));
	 * 
	 * }
	 * 
	 * } }
	 * 
	 * catch (Exception e) { System.out.println(e.getMessage()); } File DataSource =
	 * new File(
	 * "C:\\Users\\KishoreMurkhanad\\git\\repository\\IRA-test\\src\\test\\resources\\reuse\\CreateQuestion_ans.xlsx"
	 * ); FileOutputStream file_output_stream = new FileOutputStream(DataSource);
	 * XSSFWorkbook workbook = null; workbook.write(file_output_stream);
	 * file_output_stream.close(); if (col_num == -1) {
	 * System.out.println("Column you are searching for does not exist"); }
	 * 
	 * }
	 */
}
