package com.IRA.graphql.base;

import java.io.File;
import java.io.IOException;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.IRA.qa.base.TestBase;
import com.IRA.qa.base.Testbasewithoutestname;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import okhttp3.Response;

public class reuserun extends Testbasewithoutestname {
	
	@Test(priority = 1,enabled = false)
	public void Mutationforupdateassessmentwithquestions() throws IOException {
		
		File file = new File("src/test/resources/reuse/MutationformanageGroupForAssessmentQuestions.graphql");
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode root = mapper.createObjectNode();
		/*
		 * System.out.println(id); if (id.contains("null")) {
		 * System.out.println("id is null");
		 * 
		 * } root.put("id", id);
		 */
		String graphqlPayload = GraphqlTemplate.parseGraphqlwithvariables(file, null);
		System.out.println(graphqlPayload);
		String AccessToken = MutationforToken();
		Response response = prepareResponse(graphqlPayload, AccessToken);
		 long tx = response.sentRequestAtMillis();
		    long rx = response.receivedResponseAtMillis();
		    System.out.println("response time : "+(rx - tx)+" ms");
		String jsonData_MutationforCreateAssessment = response.body().string();
		System.out.println(jsonData_MutationforCreateAssessment);
		Reporter.log(jsonData_MutationforCreateAssessment);
	}
	
	@Test(priority = 2,enabled = false)
	public void Mutationforpublishassessment() throws IOException {
		
		File file = new File("src/test/resources/reuse/Mutationforpublishassessment.graphql");
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode root = mapper.createObjectNode();
		/*
		 * System.out.println(id); if (id.contains("null")) {
		 * System.out.println("id is null");
		 * 
		 * } root.put("id", id);
		 */
		String graphqlPayload = GraphqlTemplate.parseGraphqlwithvariables(file, null);
		System.out.println(graphqlPayload);
		String AccessToken = MutationforToken();
		Response response = prepareResponse(graphqlPayload, AccessToken);
		 long tx = response.sentRequestAtMillis();
		    long rx = response.receivedResponseAtMillis();
		    System.out.println("response time : "+(rx - tx)+" ms");
		String jsonData_MutationforCreateAssessment = response.body().string();
		System.out.println(jsonData_MutationforCreateAssessment);
		Reporter.log(jsonData_MutationforCreateAssessment);
	}
	
	@Test(priority = 3,enabled = false)
	public void MutationforcreateUserAssessment() throws IOException {
		
		File file = new File("src/test/resources/reuse/MutationforcreateUserAssessment.graphql");
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode root = mapper.createObjectNode();
		/*
		 * System.out.println(id); if (id.contains("null")) {
		 * System.out.println("id is null");
		 * 
		 * } root.put("id", id);
		 */
		String graphqlPayload = GraphqlTemplate.parseGraphqlwithvariables(file, null);
		System.out.println(graphqlPayload);
		String AccessToken = MutationforToken();
		Response response = prepareResponse(graphqlPayload, AccessToken);
		 long tx = response.sentRequestAtMillis();
		    long rx = response.receivedResponseAtMillis();
		    System.out.println("response time : "+(rx - tx)+" ms");
		String jsonData_MutationforCreateAssessment = response.body().string();
		System.out.println(jsonData_MutationforCreateAssessment);
		Reporter.log(jsonData_MutationforCreateAssessment);
	}
	
	@Test(priority = 4,enabled = false)
public void MutationforcreateAssessmentAttempt() throws IOException {
		
		File file = new File("src/test/resources/reuse/MutationforcreateAssessmentAttempt.graphql");
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode root = mapper.createObjectNode();
		/*
		 * System.out.println(id); if (id.contains("null")) {
		 * System.out.println("id is null");
		 * 
		 * } root.put("id", id);
		 */
		String graphqlPayload = GraphqlTemplate.parseGraphqlwithvariables(file, null);
		System.out.println(graphqlPayload);
		String AccessToken = MutationforToken();
		Response response = prepareResponse(graphqlPayload, AccessToken);
		 long tx = response.sentRequestAtMillis();
		    long rx = response.receivedResponseAtMillis();
		    System.out.println("response time : "+(rx - tx)+" ms");
		String jsonData_MutationforCreateAssessment = response.body().string();
		System.out.println(jsonData_MutationforCreateAssessment);
		Reporter.log(jsonData_MutationforCreateAssessment);
	}
	
	@Test(priority = 5,enabled = false)
	public void MutationforupdateAssessmentAttempt() throws IOException {
			
			File file = new File("src/test/resources/reuse/MutationforupdateAssessmentAttempt.graphql");
			
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode root = mapper.createObjectNode();
			/*
			 * System.out.println(id); if (id.contains("null")) {
			 * System.out.println("id is null");
			 * 
			 * } root.put("id", id);
			 */
			String graphqlPayload = GraphqlTemplate.parseGraphqlwithvariables(file, null);
			System.out.println(graphqlPayload);
			String AccessToken = MutationforToken();
			Response response = prepareResponse(graphqlPayload, AccessToken);
			 long tx = response.sentRequestAtMillis();
			    long rx = response.receivedResponseAtMillis();
			    System.out.println("response time : "+(rx - tx)+" ms");
			String jsonData_MutationforCreateAssessment = response.body().string();
			System.out.println(jsonData_MutationforCreateAssessment);
			Reporter.log(jsonData_MutationforCreateAssessment);
		}
		
	
	@Test(priority = 6)
	public void QueryforassessmentAttempt() throws IOException {
			
			File file = new File("src/test/resources/reuse/QueryforassessmentAttempt.graphql");
			
			ObjectMapper mapper = new ObjectMapper();
			ObjectNode root = mapper.createObjectNode();
			/*
			 * System.out.println(id); if (id.contains("null")) {
			 * System.out.println("id is null");
			 * 
			 * } root.put("id", id);
			 */
			String graphqlPayload = GraphqlTemplate.parseGraphqlwithvariables(file, null);
			System.out.println(graphqlPayload);
			String AccessToken = MutationforToken();
			Response response = prepareResponse(graphqlPayload, AccessToken);
			 long tx = response.sentRequestAtMillis();
			    long rx = response.receivedResponseAtMillis();
			    System.out.println("response time : "+(rx - tx)+" ms");
			String jsonData_MutationforCreateAssessment = response.body().string();
			System.out.println(jsonData_MutationforCreateAssessment);
			Reporter.log(jsonData_MutationforCreateAssessment);
		}
}

