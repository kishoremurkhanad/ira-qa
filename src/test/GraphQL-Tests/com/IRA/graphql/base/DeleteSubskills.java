package com.IRA.graphql.base;

import java.io.File;
import java.io.IOException;

import com.IRA.qa.base.TestBase;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import okhttp3.Response;

public class DeleteSubskills extends TestBase {
	
	public void deletesubskillbyid(String id) throws IOException {
		
		File file = new File("src/test/resources/MutationforDeleteSubskill.graphql");
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode root = mapper.createObjectNode();
		System.out.println(id);
		if (id.contains("null")) {
		System.out.println("id is null");

}
		root.put("id", id);
		String graphqlPayload = GraphqlTemplate.parseGraphqlwithvariables(file, root);
		System.out.println(graphqlPayload);
		String AccessToken = MutationforToken();
		Response response = prepareResponse(graphqlPayload, AccessToken);
		String jsonData = response.body().string();
		System.out.println(jsonData);
	}

}
