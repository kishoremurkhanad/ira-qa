package com.IRA.graphql.testcases;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.IRA.graphql.base.GraphqlTemplate;
import com.IRA.qa.base.SetTestName;
import com.IRA.qa.base.TestBase;
import com.IRA.qa.util.Dataprovider;

import okhttp3.Response;

public class AllSkills extends TestBase {
	
	String responsetime;

	
	
	
	@SetTestName(idx=0)
	@Test(dataProvider = "View skills Testcases", dataProviderClass = Dataprovider.class, priority = 3)
	
	
    public void ViewAllskills(String TestID, String Scenario,String Request) throws IOException {
        // Read a graphql file as an input stream
    	File file = new File(Request);
    	Reporter.log(Request);

	/*
	 * // Create a variables to pass to the graphql query ObjectNode variables = new
	 * ObjectMapper().createObjectNode(); variables.put("name", "Pikachu");
	 */

        // Now parse the graphql file to a request payload string
        String graphqlPayload = GraphqlTemplate.parseGraphqlwithoutvariables(file);
        
        String AccessToken = MutationforToken();

        // Build and trigger the request
        Response response = prepareResponse(graphqlPayload,AccessToken);
        long tx = response.sentRequestAtMillis();
        long rx = response.receivedResponseAtMillis();
        long responsetimevalue = (rx-tx);
	    responsetime = Long.toString(responsetimevalue);
		Reporter.log(responsetime);

Assert.assertEquals(response.code(), 200, "Response Code Assertion");

        String jsonData = response.body().string();
        System.out.println(jsonData);
	/*
	 * JsonNode jsonNode = new ObjectMapper().readTree(jsonData);
	 *  String AcessToken
	 * = jsonNode.get("data").get("login").get("accessToken").asText();
	 * System.out.println(AcessToken);
	 */
      
    }
}
