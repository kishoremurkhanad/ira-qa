package com.IRA.graphql.testcases;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.nio.file.Paths;

import javax.annotation.Nullable;

import org.testng.Assert;
import org.testng.ITest;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.IRA.graphql.base.DeleteSkills;
import com.IRA.graphql.base.GraphqlTemplate;
import com.IRA.qa.base.SetTestName;
import com.IRA.qa.base.TestBase;
import com.IRA.qa.util.Dataprovider;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Verify;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import okhttp3.Response;

public class AddSkills extends TestBase implements ITest {
	private String newTestName = "";
	DeleteSkills delskills = new DeleteSkills();
	//private ThreadLocal<String> testName = new ThreadLocal<>();
	
	 
	/**
	 * @throws IOException
	 */

	@SetTestName(idx=0)
	@Test(dataProvider = "Add skills Testcases", dataProviderClass = Dataprovider.class, priority = 1)

	public void AddskillTcids(String TestID, String Scenario, String Request,String Name,String Description)
			throws IOException {
		String responsetime;

		// Read a graphql file as an input stream

		//Reporter.log("TestcaseID is " + TestID);
		File file = new File(Request);

		// getResourceAsStream("src/test/resources/MutationforAddSkill.graphql");

		/*
		 * // Create a variables to pass to the graphql query ObjectNode variables = new
		 * ObjectMapper().createObjectNode();
		 * 
		 * 
		 * 
		 * variables.put("name", "HydTestSkill-1"); String Node =
		 * variables.toPrettyString(); System.out.println(Node);
		 */

		// Now parse the graphql file to a request payload string

		ObjectMapper mapper = new ObjectMapper();
		ObjectNode root = mapper.createObjectNode();
		System.out.println(Name);
		System.out.println(Description);
		if (Name.contains("null")) {
			Name = " ";
		}
		if (Description.contains("null")) {
			Description = "";
		}
		System.out.println(Name);
		System.out.println(Description);
		root.put("name", Name);
		root.put("description", Description);
		String graphqlPayload = GraphqlTemplate.parseGraphqlwithvariables(file, root);
		System.out.println(TestID);
		System.out.println(graphqlPayload);

		String AccessToken = MutationforToken();
		Response response = prepareResponse(graphqlPayload, AccessToken);
		 long tx = response.sentRequestAtMillis();
		    long rx = response.receivedResponseAtMillis();
		    long responsetimevalue = (rx-tx);
		   responsetime = Long.toString(responsetimevalue);
		   

		    System.out.println("response time : "+(rx - tx)+" ms");
		String jsonData = response.body().string();
	    JsonNode jsonNode = new ObjectMapper().readTree(jsonData);
   	    System.out.println(jsonNode);
   	   
   	 Reporter.log(graphqlPayload);
		Reporter.log(responsetime);

	 
	    
	    switch (TestID) {
		
        case "TC_API_MTCSKILL_1":
        	
        	
        	Assert.assertEquals(response.code(), 200, "Response Code Assertion");
           	String errorsnode_TC_API_MTCSKILL_1 = jsonNode.get("errors").toPrettyString();
       	    int errorslength_TC_API_MTCSKILL_1 = errorsnode_TC_API_MTCSKILL_1.length();
       	    System.out.println(errorslength_TC_API_MTCSKILL_1);
       	    String errors_TC_API_MTCSKILL_1 = errorsnode_TC_API_MTCSKILL_1.substring(1, errorslength_TC_API_MTCSKILL_1-1);
       	    JsonNode Errors_TC_API_MTCSKILL_1 = new ObjectMapper().readTree(errors_TC_API_MTCSKILL_1);
       	    String Message_TC_API_MTCSKILL_1 = Errors_TC_API_MTCSKILL_1.get("message").asText();
       	    System.out.println(Message_TC_API_MTCSKILL_1);
       	    Assert.assertEquals(Message_TC_API_MTCSKILL_1, "Skill already exist");
       	    break;

        case "TC_API_MTCSKILL_2":
        	
        Assert.assertEquals(response.code(), 200, "Response Code Assertion");
        String skillid_TC_API_MTCSKILL_2 = jsonNode.get("data").get("createSkill").get("id").asText();
        Verify.verifyNotNull(skillid_TC_API_MTCSKILL_2);
        String skillname_TC_API_MTCSKILL_2_actual = jsonNode.get("data").get("createSkill").get("name").asText();
        Verify.verify(true, skillname_TC_API_MTCSKILL_2_actual, Name);
        //Assert.assertEquals(skillname_actual, Name);
        System.out.println(skillname_TC_API_MTCSKILL_2_actual);
        System.out.println(skillid_TC_API_MTCSKILL_2);
        delskills.deleteskillbyid(skillid_TC_API_MTCSKILL_2);
       	break;

        case "TC_API_MTCSKILL_3":
        	
        	Assert.assertEquals(response.code(), 200, "Response Code Assertion");
         	String errorsnode_TC_API_MTCSKILL_3 = jsonNode.get("errors").toPrettyString();
       	    int errorslength_TC_API_MTCSKILL_3 = errorsnode_TC_API_MTCSKILL_3.length();
       	    System.out.println(errorslength_TC_API_MTCSKILL_3);
       	    String errors_TC_API_MTCSKILL_3 = errorsnode_TC_API_MTCSKILL_3.substring(1, errorslength_TC_API_MTCSKILL_3-1);
       	    JsonNode Errors_TC_API_MTCSKILL_3 = new ObjectMapper().readTree(errors_TC_API_MTCSKILL_3);
       	    String Message_TC_API_MTCSKILL_3 = Errors_TC_API_MTCSKILL_3.get("message").asText();
       	    System.out.println(Message_TC_API_MTCSKILL_3);
       	    Assert.assertEquals(Message_TC_API_MTCSKILL_3, "Skill already exist");
       	    break;

        case "TC_API_MTCSKILL_4":
        	
        	Assert.assertEquals(response.code(), 200, "Response Code Assertion");        	
            String skillid_TC_API_MTCSKILL_4 = jsonNode.get("data").get("createSkill").get("id").asText();
       		Response response_TC_API_MTCSKILL_4 = prepareResponse(graphqlPayload, AccessToken);
    		String jsonData_TC_API_MTCSKILL_4 = response_TC_API_MTCSKILL_4.body().string();
    	    JsonNode jsonNode_TC_API_MTCSKILL_4 = new ObjectMapper().readTree(jsonData_TC_API_MTCSKILL_4);
          	String errorsnode_TC_API_MTCSKILL_4 = jsonNode_TC_API_MTCSKILL_4.get("errors").toPrettyString();
       	    int errorslength_TC_API_MTCSKILL_4 = errorsnode_TC_API_MTCSKILL_4.length();
       	    System.out.println(errorslength_TC_API_MTCSKILL_4);
       	    String errors_TC_API_MTCSKILL_4 = errorsnode_TC_API_MTCSKILL_4.substring(1, errorslength_TC_API_MTCSKILL_4-1);
       	    JsonNode Errors_TC_API_MTCSKILL_4 = new ObjectMapper().readTree(errors_TC_API_MTCSKILL_4);
       	    String Message_TC_API_MTCSKILL_4 = Errors_TC_API_MTCSKILL_4.get("message").asText();
       	    System.out.println(Message_TC_API_MTCSKILL_4);
       	    Assert.assertEquals(Message_TC_API_MTCSKILL_4, "Skill already exist");
            delskills.deleteskillbyid(skillid_TC_API_MTCSKILL_4);

       	    break;
          
        case "TC_API_MTCSKILL_5":
        	
        	 Assert.assertEquals(response.code(), 200, "Response Code Assertion");
             String skillid_TC_API_MTCSKILL_5 = jsonNode.get("data").get("createSkill").get("id").asText();
             Verify.verifyNotNull(skillid_TC_API_MTCSKILL_5);
             String skillname_TC_API_MTCSKILL_5_actual = jsonNode.get("data").get("createSkill").get("name").asText();
             Verify.verify(true, skillname_TC_API_MTCSKILL_5_actual, Name);
         //    Assert.assertEquals(skillname_actual, Name);
             System.out.println(skillname_TC_API_MTCSKILL_5_actual);
             System.out.println(skillid_TC_API_MTCSKILL_5);
             delskills.deleteskillbyid(skillid_TC_API_MTCSKILL_5);
            	   break;
        	
        case "TC_API_MTCSKILL_6":
        	
        	Assert.assertEquals(response.code(), 200, "Response Code Assertion");        	
            String skillid_TC_API_MTCSKILL_6= jsonNode.get("data").get("createSkill").get("id").asText();
       		Response response_TC_API_MTCSKILL_6= prepareResponse(graphqlPayload, AccessToken);
    		String jsonData_TC_API_MTCSKILL_6= response_TC_API_MTCSKILL_6.body().string();
    	    JsonNode jsonNode_TC_API_MTCSKILL_6= new ObjectMapper().readTree(jsonData_TC_API_MTCSKILL_6);
          	String errorsnode_TC_API_MTCSKILL_6= jsonNode_TC_API_MTCSKILL_6.get("errors").toPrettyString();
       	    int errorslength_TC_API_MTCSKILL_6= errorsnode_TC_API_MTCSKILL_6.length();
       	    System.out.println(errorslength_TC_API_MTCSKILL_6);
       	    String errors_TC_API_MTCSKILL_6= errorsnode_TC_API_MTCSKILL_6.substring(1, errorslength_TC_API_MTCSKILL_6-1);
       	    JsonNode Errors_TC_API_MTCSKILL_6= new ObjectMapper().readTree(errors_TC_API_MTCSKILL_6);
       	    String Message_TC_API_MTCSKILL_6= Errors_TC_API_MTCSKILL_6.get("message").asText();
       	    System.out.println(Message_TC_API_MTCSKILL_6);
       	    Assert.assertEquals(Message_TC_API_MTCSKILL_6, "Skill already exist");
            delskills.deleteskillbyid(skillid_TC_API_MTCSKILL_6);
       	    break;

        default: 
        	
          System.out.println("Add a new case");
          break;
       }       
   }

	

	



	 
}
