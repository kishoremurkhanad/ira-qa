package com.IRA.graphql.testcases;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import org.testng.Assert;
import org.testng.ITest;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import com.IRA.graphql.base.DeleteSubskills;
import com.IRA.graphql.base.GraphqlTemplate;
import com.IRA.qa.base.SetTestName;
import com.IRA.qa.base.TestBase;
import com.IRA.qa.util.Dataprovider;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Verify;

import okhttp3.Response;

public class AddSubskills  extends TestBase implements ITest {
	
	//private String newTestName = "";
	DeleteSubskills delsubskills = new DeleteSubskills();
	String responsetime;
	
	//private ThreadLocal<String> testName = new ThreadLocal<>();
	/*
	 * @BeforeMethod(alwaysRun=true) public void getTheNameFromParemeters(Method
	 * method, Object [] TestID,Object[] Scenario) { SetTestName setTestName =
	 * method.getAnnotation(SetTestName.class); String testCaseName =
	 * (String)TestID[setTestName.idx()] + "--"+(String) Scenario[1];
	 * System.out.println(testCaseName); setTestName(testCaseName); }
	 * 
	 * 
	 * private void setTestName(String newTestName){ this.newTestName = newTestName;
	 * }
	 * 
	 * public String getTestName() {
	 * 
	 * return newTestName; }
	 * 
	 * 
	 */
	/**
	 * @throws IOException
	 */

	@SetTestName(idx=0)
	@Test(dataProvider = "Add Subskills Testcases", dataProviderClass = Dataprovider.class, priority = 2)

	public void AddsubskillTcids(String TestID, String Scenario, String Request,String Name,String SkillId)
			throws IOException {
		// Read a graphql file as an input stream

		
		Reporter.log(Request);
		File file = new File(Request);

		// getResourceAsStream("src/test/resources/MutationforAddSkill.graphql");

		/*
		 * // Create a variables to pass to the graphql query ObjectNode variables = new
		 * ObjectMapper().createObjectNode();
		 * 
		 * 
		 * 
		 * variables.put("name", "HydTestSkill-1"); String Node =
		 * variables.toPrettyString(); System.out.println(Node);
		 */

		// Now parse the graphql file to a request payload string

		ObjectMapper mapper = new ObjectMapper();
		ObjectNode root = mapper.createObjectNode();
		System.out.println(Name);
		System.out.println(SkillId);
		if (Name.contains("null")) {
			Name = " ";
		}
		if (SkillId.contains("null")) {
			SkillId = "";
		}
		System.out.println(Name);
		System.out.println(SkillId);
		root.put("name", Name);
		root.put("skill", SkillId);
		String graphqlPayload = GraphqlTemplate.parseGraphqlwithvariables(file, root);
		System.out.println(TestID);
		System.out.println(graphqlPayload);

		String AccessToken = MutationforToken();
		Response response = prepareResponse(graphqlPayload, AccessToken);
		String jsonData = response.body().string();
		 long tx = response.sentRequestAtMillis();
		    long rx = response.receivedResponseAtMillis();
		    long responsetimevalue = (rx-tx);
		    responsetime = Long.toString(responsetimevalue);
			Reporter.log(responsetime);
		    JsonNode jsonNode = new ObjectMapper().readTree(jsonData);
   	    System.out.println(jsonNode);

	 
	    
	    switch (TestID) {
		
        case "TC_API_MTCSUBSKILL_1":
        	
        	

       	 Assert.assertEquals(response.code(), 200, "Response Code Assertion");
         String skillid_TC_API_MTCSUBSKILL_1 = jsonNode.get("data").get("createSubSkill").get("id").asText();
         Verify.verifyNotNull(skillid_TC_API_MTCSUBSKILL_1);
         String skillname_TC_API_MTCSUBSKILL_1_actual = jsonNode.get("data").get("createSubSkill").get("name").asText();
         Verify.verify(true, skillname_TC_API_MTCSUBSKILL_1_actual, Name);
     //    Assert.assertEquals(skillname_actual, Name);
         System.out.println(skillname_TC_API_MTCSUBSKILL_1_actual);
         System.out.println(skillid_TC_API_MTCSUBSKILL_1);
         delsubskills.deletesubskillbyid(skillid_TC_API_MTCSUBSKILL_1);
        	   break;
        	   
        case "TC_API_MTCSUBSKILL_2":
        	
        	    Assert.assertEquals(response.code(), 200, "Response Code Assertion");
                String errorsnode_TC_API_MTCSUBSKILL_2 = jsonNode.get("errors").toPrettyString();
        	    int errorslength_TC_API_MTCSUBSKILL_2 = errorsnode_TC_API_MTCSUBSKILL_2.length();
        	    System.out.println(errorslength_TC_API_MTCSUBSKILL_2);
        	    String errors_TC_API_MTCSUBSKILL_2 = errorsnode_TC_API_MTCSUBSKILL_2.substring(1, errorslength_TC_API_MTCSUBSKILL_2-1);
        	    JsonNode Errors_TC_API_MTCSUBSKILL_2 = new ObjectMapper().readTree(errors_TC_API_MTCSUBSKILL_2);
        	    String Message_TC_API_MTCSUBSKILL_2 = Errors_TC_API_MTCSUBSKILL_2.get("message").asText();
        	    System.out.println(Message_TC_API_MTCSUBSKILL_2);
        	    Assert.assertEquals(Message_TC_API_MTCSUBSKILL_2, "CastError: Cast to ObjectId failed for value \"\" at path \"_id\" for model \"Skill\"]");
        	    break;

        case "TC_API_MTCSUBSKILL_3":
        	
        	
        	Assert.assertEquals(response.code(), 200, "Response Code Assertion");
         	String errorsnode_TC_API_MTCSUBSKILL_3 = jsonNode.get("errors").toPrettyString();
       	    int errorslength_TC_API_MTCSUBSKILL_3 = errorsnode_TC_API_MTCSUBSKILL_3.length();
       	    System.out.println(errorslength_TC_API_MTCSUBSKILL_3);
       	    String errors_TC_API_MTCSUBSKILL_3 = errorsnode_TC_API_MTCSUBSKILL_3.substring(1, errorslength_TC_API_MTCSUBSKILL_3-1);
       	    JsonNode Errors_TC_API_MTCSUBSKILL_3 = new ObjectMapper().readTree(errors_TC_API_MTCSUBSKILL_3);
       	    String Message_TC_API_MTCSUBSKILL_3 = Errors_TC_API_MTCSUBSKILL_3.get("message").asText();
       	    System.out.println(Message_TC_API_MTCSUBSKILL_3);
       	    Assert.assertEquals(Message_TC_API_MTCSUBSKILL_3, "Sub Skill already exist");
       	    break;

        case "TC_API_MTCSUBSKILL_4":
        	
        	 Assert.assertEquals(response.code(), 200, "Response Code Assertion");
             String skillid_TC_API_MTCSUBSKILL_4 = jsonNode.get("data").get("createSubSkill").get("id").asText();
             Verify.verifyNotNull(skillid_TC_API_MTCSUBSKILL_4);
             String skillname_TC_API_MTCSUBSKILL_4_actual = jsonNode.get("data").get("createSubSkill").get("name").asText();
             Verify.verify(true, skillname_TC_API_MTCSUBSKILL_4_actual, Name);
         //    Assert.assertEquals(skillname_actual, Name);
             System.out.println(skillname_TC_API_MTCSUBSKILL_4_actual);
             System.out.println(skillid_TC_API_MTCSUBSKILL_4);
             delsubskills.deletesubskillbyid(skillid_TC_API_MTCSUBSKILL_4);
            	   break;
          
        case "TC_API_MTCSUBSKILL_5":
        	
        	 Assert.assertEquals(response.code(), 200, "Response Code Assertion");
             String skillid_TC_API_MTCSUBSKILL_5 = jsonNode.get("data").get("createSubSkill").get("id").asText();
             Verify.verifyNotNull(skillid_TC_API_MTCSUBSKILL_5);
             String skillname_TC_API_MTCSUBSKILL_5_actual = jsonNode.get("data").get("createSubSkill").get("name").asText();
             Verify.verify(true, skillname_TC_API_MTCSUBSKILL_5_actual, Name);
         //    Assert.assertEquals(skillname_actual, Name);
             System.out.println(skillname_TC_API_MTCSUBSKILL_5_actual);
             System.out.println(skillid_TC_API_MTCSUBSKILL_5);
             delsubskills.deletesubskillbyid(skillid_TC_API_MTCSUBSKILL_5);
            	   break;
        	
        case "TC_API_MTCSUBSKILL_6":
        	
        	Assert.assertEquals(response.code(), 200, "Response Code Assertion");
            String errorsnode_TC_API_MTCSUBSKILL_6 = jsonNode.get("errors").toPrettyString();
    	    int errorslength_TC_API_MTCSUBSKILL_6 = errorsnode_TC_API_MTCSUBSKILL_6.length();
    	    System.out.println(errorslength_TC_API_MTCSUBSKILL_6);
    	    String errors_TC_API_MTCSUBSKILL_6 = errorsnode_TC_API_MTCSUBSKILL_6.substring(1, errorslength_TC_API_MTCSUBSKILL_6-1);
    	    JsonNode Errors_TC_API_MTCSUBSKILL_6 = new ObjectMapper().readTree(errors_TC_API_MTCSUBSKILL_6);
    	    String Message_TC_API_MTCSUBSKILL_6 = Errors_TC_API_MTCSUBSKILL_6.get("message").asText();
    	    System.out.println(Message_TC_API_MTCSUBSKILL_6);
    	    Assert.assertEquals(Message_TC_API_MTCSUBSKILL_6, "CastError: Cast to ObjectId failed for value");
          

    	    break;
    	    
        case "TC_API_MTCSUBSKILL_7":
        	
        	Assert.assertEquals(response.code(), 200, "Response Code Assertion");        	
        	String errorsnode_TC_API_MTCSUBSKILL_7 = jsonNode.get("errors").toPrettyString();
       	    int errorslength_TC_API_MTCSUBSKILL_7 = errorsnode_TC_API_MTCSUBSKILL_7.length();
       	    System.out.println(errorslength_TC_API_MTCSUBSKILL_7);
       	    String errors_TC_API_MTCSUBSKILL_7 = errorsnode_TC_API_MTCSUBSKILL_7.substring(1, errorslength_TC_API_MTCSUBSKILL_7-1);
       	    JsonNode Errors_TC_API_MTCSUBSKILL_7 = new ObjectMapper().readTree(errors_TC_API_MTCSUBSKILL_7);
       	    String Message_TC_API_MTCSUBSKILL_7 = Errors_TC_API_MTCSUBSKILL_7.get("message").asText();
       	    System.out.println(Message_TC_API_MTCSUBSKILL_7);
       	    Assert.assertEquals(Message_TC_API_MTCSUBSKILL_7, "Sub Skill already exist");
       	    break;

        default: 
        	
          System.out.println("Add a new case");
          break;
       }       
   }

	

	




}
