
package com.IRA.ExtentReportListener;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.xml.XmlSuite;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ExtentReporterNG implements IReporter {
	private ExtentReports extent;
	
	
	
	

	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites,
			String outputDirectory) {
		extent = new ExtentReports(outputDirectory + File.separator
				+ "APIRunReport.html", true);

		for (ISuite suite : suites) {
			Map<String, ISuiteResult> result = suite.getResults();
	//		Map<String, ISuiteResult> result = sortByValue(ISuiteResult.class.getName());
			System.out.println(result.values());
			

			for (ISuiteResult r : result.values()) {
				ITestContext context = r.getTestContext();
				
				String responsetime;
				try {
					
					buildTestNodes(context.getPassedTests(), LogStatus.PASS);
					buildTestNodes(context.getFailedTests(), LogStatus.FAIL);
					buildTestNodes(context.getSkippedTests(), LogStatus.SKIP);
				} catch (NoSuchFieldException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
						
						

			
			}
		}

		extent.flush();
		//extent.close();
	}
	
	
	private Map<String, ISuiteResult> sortByValue(String name) {
		// TODO Auto-generated method stub
		return null;
	}


	
	@AfterMethod(alwaysRun = true)
	private void buildTestNodes(IResultMap tests, LogStatus status) throws NoSuchFieldException, SecurityException {
		ExtentTest test;
	

		
		
	//	Collections.sort(null);

		if (tests.size() > 0) {
			
			List<ITestResult> resultList = new LinkedList<ITestResult>(tests.getAllResults());
			
			class ResultComparator implements Comparator<ITestResult> {
	            public int compare(ITestResult r1, ITestResult r2) {
	                return getTime(r1.getStartMillis()).compareTo(getTime(r2.getStartMillis()));
	                            }
	        }

	        Collections.sort(resultList , new ResultComparator ());
			
			for (ITestResult result : resultList) {
				
				if(result.getName() != null) {
					String testname = result.getName();
				}
				else
				{
					String testname = result.getMethod().getMethodName();
				}
				String testname = result.getName();
			
				
				test = extent.startTest(testname);
				test.setStartedTime(getTime(result.getStartMillis()));
				test.setEndedTime(getTime(result.getEndMillis()));

				for (String group : result.getMethod().getGroups())
					test.assignCategory(group);
				
				String Request= Reporter.getOutput(result).get(0).toString();
				String Responsetime= Reporter.getOutput(result).get(1).toString();


				if (result.getThrowable() != null) {
					test.log(status, result.getThrowable());
				} else {
					
					
					test.log(status, "Test " + status.toString().toLowerCase()
							+ "ed");
				//	test.log(status,"API Request used for this test is : "+Request);
					test.log(status,"Response time for the API "+ Request +" is : "+Responsetime+" ms");
					
				
				}
				
			

				extent.endTest(test);
			}
		}
	}

	private Date getTime(long millis) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
		return calendar.getTime();
	}
}