package com.IRA.qa.util;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;


public class Dataprovider {
	
	
	//Data Provider For View All skills Testcases

    @DataProvider(name = "View skills Testcases")
    public static Object[][] Viewskillstcs() {
        Object[][] arrayObject = getViewskillstcs("C:\\Users\\KishoreMurkhanad\\git\\repository\\IRA-test\\src\\test\\resources\\IRA_API_Testcasesanddata_sheet.xlsx", "View_skills");
        return arrayObject;
    }

    /**
     * @param fileName
     * @param sheetName
     * @return
     */
    public static String[][] getViewskillstcs(String fileName, String sheetName) {
        String[][] arrayExcelData = null;
        try {
            File DataSource = new File(fileName);
            FileInputStream fis = new FileInputStream(DataSource);
            Workbook wb = new XSSFWorkbook(fis);
            Sheet sh = wb.getSheet(sheetName);
            
            int totalNoOfRows = sh.getLastRowNum();
            System.out.println(totalNoOfRows+1);
            int totalNoOfCols = sh.getRow(0).getLastCellNum();
            System.out.println(totalNoOfCols);

            
            arrayExcelData = new String[totalNoOfRows][totalNoOfCols];

            for (int i = 1; i < totalNoOfRows+1; i++) {

                for (int j = 0; j < totalNoOfCols; j++) {
                //	Cell cell = sh.getRow(i).getCell(j,  org.apache.poi.ss.usermodel.Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
                  //  arrayExcelData[i - 1][j] = cell.getStringCellValue();
                    
                    arrayExcelData[i - 1][j] =   sh.getRow(i).getCell(j).getStringCellValue();
        	     //  System.out.println(Arrays.deepToString(arrayExcelData));

                }

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            e.printStackTrace();
        } 
     //   System.out.println(Arrays.deepToString(arrayExcelData));
        return arrayExcelData;

    }
    
    
	
    //Data Provider For AddSkills Testcases

	    @DataProvider(name = "Add skills Testcases")
	    public static Object[][] Addskillstcs() {
	        Object[][] arrayObject = getAddskillstcs("C:\\Users\\KishoreMurkhanad\\git\\repository\\IRA-test\\src\\test\\resources\\IRA_API_Testcasesanddata_sheet.xlsx", "Skill_Testcases");
	        return arrayObject;
	    }

	    /**
	     * @param fileName
	     * @param sheetName
	     * @return
	     */
	    public static String[][] getAddskillstcs(String fileName, String sheetName) {
	        String[][] arrayExcelData = null;
	        try {
	            File DataSource = new File(fileName);
	            FileInputStream fis = new FileInputStream(DataSource);
	            Workbook wb = new XSSFWorkbook(fis);
	            Sheet sh = wb.getSheet(sheetName);
	            
	            int totalNoOfRows = sh.getLastRowNum();
	            System.out.println(totalNoOfRows+1);
	            int totalNoOfCols = sh.getRow(0).getLastCellNum();
	            System.out.println(totalNoOfCols);

	            
	            arrayExcelData = new String[totalNoOfRows][totalNoOfCols];

	            for (int i = 1; i < totalNoOfRows+1; i++) {

	                for (int j = 0; j < totalNoOfCols; j++) {
	                //	Cell cell = sh.getRow(i).getCell(j,  org.apache.poi.ss.usermodel.Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
	                  //  arrayExcelData[i - 1][j] = cell.getStringCellValue();
	                    
	                    arrayExcelData[i - 1][j] =   sh.getRow(i).getCell(j).getStringCellValue();
	        	     //  System.out.println(Arrays.deepToString(arrayExcelData));

	                }

	            }

	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	            e.printStackTrace();
	        } 
	     //   System.out.println(Arrays.deepToString(arrayExcelData));
	        return arrayExcelData;

	    }
	    
	    
	    //Data Provider For Add SubSkill Testcases

	    @DataProvider(name = "Add Subskills Testcases")
	    public static Object[][] Addsubskillstcs() {
	        Object[][] arrayObject = getAddsubskillstcs("C:\\Users\\KishoreMurkhanad\\git\\repository\\IRA-test\\src\\test\\resources\\IRA_API_Testcasesanddata_sheet.xlsx", "SubSkill_Testcases");
	        return arrayObject;
	    }

	    /**
	     * @param fileName
	     * @param sheetName
	     * @return
	     */
	    public static String[][] getAddsubskillstcs(String fileName, String sheetName) {
	        String[][] arrayExcelData = null;
	        try {
	            File DataSource = new File(fileName);
	            FileInputStream fis = new FileInputStream(DataSource);
	            Workbook wb = new XSSFWorkbook(fis);
	            Sheet sh = wb.getSheet(sheetName);
	            
	            int totalNoOfRows = sh.getLastRowNum();
	            System.out.println(totalNoOfRows+1);
	            int totalNoOfCols = sh.getRow(1).getLastCellNum();
	            System.out.println(totalNoOfCols);

	            
	            arrayExcelData = new String[totalNoOfRows][totalNoOfCols];

	            for (int i = 1; i < totalNoOfRows+1; i++) {

	                for (int j = 0; j < totalNoOfCols; j++) {
	                    arrayExcelData[i - 1][j] = sh.getRow(i).getCell(j).getStringCellValue();
	        	       // System.out.println(Arrays.deepToString(arrayExcelData));

	                }

	            }

	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	            e.printStackTrace();
	        } 
	       // System.out.println(Arrays.deepToString(arrayExcelData));
	        return arrayExcelData;

	    }
	    
	    //Data Provider For Add Competencies Testcases

	    @DataProvider(name = "Add Competencies Testcases")
	    public static Object[][] AddCompetenciestcs() {
	        Object[][] arrayObject = getAddCompetenciestcs("C:\\Users\\KishoreMurkhanad\\git\\repository\\IRA-test\\src\\test\\resources\\IRA_API_Testcasesanddata_sheet.xlsx", "Competency_Testcases");
	        return arrayObject;
	    }

	    /**
	     * @param fileName
	     * @param sheetName
	     * @return
	     */
	    public static String[][] getAddCompetenciestcs(String fileName, String sheetName) {
	        String[][] arrayExcelData = null;
	        try {
	            File DataSource = new File(fileName);
	            FileInputStream fis = new FileInputStream(DataSource);
	            Workbook wb = new XSSFWorkbook(fis);
	            Sheet sh = wb.getSheet(sheetName);
	            
	            int totalNoOfRows = sh.getLastRowNum();
	            System.out.println(totalNoOfRows+1);
	            int totalNoOfCols = sh.getRow(1).getLastCellNum();
	            System.out.println(totalNoOfCols);

	            
	            arrayExcelData = new String[totalNoOfRows][totalNoOfCols];

	            for (int i = 1; i < totalNoOfRows+1; i++) {

	                for (int j = 0; j < totalNoOfCols; j++) {
	                    arrayExcelData[i - 1][j] = sh.getRow(i).getCell(j).getStringCellValue();
	        	       // System.out.println(Arrays.deepToString(arrayExcelData));

	                }

	            }

	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	            e.printStackTrace();
	        } 
	       // System.out.println(Arrays.deepToString(arrayExcelData));
	        return arrayExcelData;

	    }
	    
	  //Data Provider For View All skills Testcases

	    @DataProvider(name = "Add Questions")
	    public static Object[][] Addquestions() {
	        Object[][] arrayObject = getAddquestions("C:\\Users\\KishoreMurkhanad\\git\\repository\\IRA-test\\src\\test\\resources\\reuse\\CreateQuestion.xlsx", "Create_Question");
	        return arrayObject;
	    }

	    /**
	     * @param fileName
	     * @param sheetName
	     * @return
	     */
	    public static String[][] getAddquestions(String fileName, String sheetName) {
	        String[][] arrayExcelData = null;
	        try {
	            File DataSource = new File(fileName);
	            FileInputStream fis = new FileInputStream(DataSource);
	            Workbook wb = new XSSFWorkbook(fis);
	            Sheet sh = wb.getSheet(sheetName);
	            
	            int totalNoOfRows = sh.getLastRowNum();
	            System.out.println(totalNoOfRows+1);
	            int totalNoOfCols = sh.getRow(0).getLastCellNum();
	            System.out.println(totalNoOfCols);

	            
	            arrayExcelData = new String[totalNoOfRows][totalNoOfCols];

	            for (int i = 1; i < totalNoOfRows+1; i++) {

	                for (int j = 0; j < totalNoOfCols; j++) {
	                //	Cell cell = sh.getRow(i).getCell(j,  org.apache.poi.ss.usermodel.Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
	                  //  arrayExcelData[i - 1][j] = cell.getStringCellValue();
	                    
	                    arrayExcelData[i - 1][j] =   sh.getRow(i).getCell(j).getStringCellValue();
	        	     //  System.out.println(Arrays.deepToString(arrayExcelData));

	                }

	            }

	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	            e.printStackTrace();
	        } 
	     //   System.out.println(Arrays.deepToString(arrayExcelData));
	        return arrayExcelData;

	    }
	    
	    @DataProvider(name = "Create Assessments")
	    public static Object[][] CreateAssessments() {
	        Object[][] arrayObject = getCreateAssessments("C:\\Users\\KishoreMurkhanad\\git\\repository\\IRA-test\\src\\test\\resources\\reuse\\CreateAssessment.xlsx", "Create_Assessment");
	        return arrayObject;
	    }

	    /**
	     * @param fileName
	     * @param sheetName
	     * @return
	     */
	    public static String[][] getCreateAssessments(String fileName, String sheetName) {
	        String[][] arrayExcelData = null;
	        try {
	            File DataSource = new File(fileName);
	            FileInputStream fis = new FileInputStream(DataSource);
	            Workbook wb = new XSSFWorkbook(fis);
	            Sheet sh = wb.getSheet(sheetName);
	            
	            int totalNoOfRows = sh.getLastRowNum();
	            System.out.println(totalNoOfRows+1);
	            int totalNoOfCols = sh.getRow(0).getLastCellNum();
	            System.out.println(totalNoOfCols);

	            
	            arrayExcelData = new String[totalNoOfRows][totalNoOfCols];

	            for (int i = 1; i < totalNoOfRows+1; i++) {

	                for (int j = 0; j < totalNoOfCols; j++) {
	                //	Cell cell = sh.getRow(i).getCell(j,  org.apache.poi.ss.usermodel.Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
	                  //  arrayExcelData[i - 1][j] = cell.getStringCellValue();
	                    
	                    arrayExcelData[i - 1][j] =   sh.getRow(i).getCell(j).getStringCellValue();
	        	     //  System.out.println(Arrays.deepToString(arrayExcelData));

	                }

	            }

	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	            e.printStackTrace();
	        } 
	     //   System.out.println(Arrays.deepToString(arrayExcelData));
	        return arrayExcelData;

	    }
	    

	    

}
	



