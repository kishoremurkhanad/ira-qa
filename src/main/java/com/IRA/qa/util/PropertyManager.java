package com.IRA.qa.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by KMurkhanad on 1/4/2016.
 */
public class PropertyManager {
    private static final Properties PROPERTY = new Properties();
    private static final String FRAMEWORKPROPERTIESPATH = "/IRA-test/src/main/java/com/IRA/qa/config/Locators.properties";
    //private static final Logger LOGGER = Log.error();

    public static Properties loadPropertyFile(String propertyToLoad) {
        try {
            PROPERTY.load(new FileInputStream(FRAMEWORKPROPERTIESPATH));
        } catch (IOException io) {
            //LOGGER.info("IOException in the loadFrameworkPropertyFile() method of the PropertyManager class",io);
            Runtime.getRuntime().halt(0);
        }
        return PROPERTY;
    }
}
