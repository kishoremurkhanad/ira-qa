package com.IRA.qa.base;

import java.lang.annotation.*;




@Retention(RetentionPolicy.RUNTIME)	
public @interface SetTestName {
	    int idx() default 0;
	}


