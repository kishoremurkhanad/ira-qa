package com.IRA.qa.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Assert;
import org.testng.ITest;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.IRA.graphql.base.GraphqlTemplate;
import com.IRA.qa.util.Dataprovider;
import com.IRA.qa.util.TestUtil;
import com.IRA.qa.util.WebEventListener;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TestBase extends Dataprovider implements ITest{
	
	public static WebDriver driver;
	public static Properties prop;
	public static Properties Locatorproperties;
	public  static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
    private static final OkHttpClient client = new OkHttpClient();
    private final String graphqlUri = "http://test.kh3ira.com:7000/graphql";
    protected String newTestName = "";
    
    
    
    @BeforeMethod(alwaysRun=true) 
	  
	 public void getTheNameFromParemeters (Method  method,@Nullable Object [] TestID,@Nullable Object[] Scenario)
    { 
		  SetTestName setTestName =	  method.getAnnotation(SetTestName.class);
String TestID_name = TestID.toString();		  
System.out.println(TestID_name);
		  if (TestID_name != null) {
		 String testCaseName = (String)TestID[setTestName.idx()] + " - "+(String) Scenario[1];
		 // String testCaseName = (String)TestID[setTestName.idx()] ;
		  System.out.println(testCaseName);
		  setTestName(testCaseName);
		  }
		  else {
			  
			  String testCaseName = method.getName();
			  System.out.println(testCaseName);
			  setTestName(testCaseName);
		  }
		  
		  }
	 

		 private void setTestName(String newTestName){
		        this.newTestName = newTestName;
		    }
		 
		 @Override
		    public String getTestName() {

		        return newTestName;
		    }
    
    
	public TestBase(){
		try {
			prop = new Properties();
			FileInputStream ip_config = new FileInputStream(System.getProperty("user.dir")+ "/src/main/java/com/IRA"
					+ "/qa/config/config.properties");
			prop.load(ip_config);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			Locatorproperties = new Properties();
			FileInputStream ip_locators = new FileInputStream(System.getProperty("user.dir")+ "/src/main/java/com/IRA"
					+ "/qa/config/Locators.properties");
			Locatorproperties.load(ip_locators);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void initialization(){
		String browserName = prop.getProperty("browser");
		
		if(browserName.equals("chrome")){
			System.setProperty("webdriver.chrome.driver", "D:/browser drivers/chromedriver.exe");	
			driver = new ChromeDriver(); 
		}
		else if(browserName.equals("FF")){
			System.setProperty("webdriver.gecko.driver", "D:/browser drivers/geckodriver.exe");	
			driver = new FirefoxDriver(); 
		}
		
		
		e_driver = new EventFiringWebDriver(driver);
		// Now create object of EventListerHandler to register it with EventFiringWebDriver
		eventListener = new WebEventListener();
		e_driver.register(eventListener);
		driver = e_driver;
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		
		driver.get(prop.getProperty("url"));
		
	}
	
	

	

	    public Response prepareResponseforauthtoken(String graphqlPayload) throws IOException {
	        RequestBody Reqbody = RequestBody.create(MediaType.get("application/json; charset=utf-8"), graphqlPayload);
	       	 Request request = new Request.Builder().url(graphqlUri).post(Reqbody).build();
	         String req_str = request.toString();
		        System.out.println(req_str);
	        return client.newCall(request).execute();
	    }
	    
	    public Response prepareResponse(String graphqlPayload, String Authtoken) throws IOException {
	        RequestBody Reqbody = RequestBody.create(MediaType.get("application/json; charset=utf-8"), graphqlPayload);
	       	 Request request = new Request.Builder().url(graphqlUri).post(Reqbody).addHeader("Authorization", "Bearer "+Authtoken).build();
	        String req_str = request.toString();
	        System.out.println(req_str);
	        return client.newCall(request).execute();
	    }

	    
	    public  String MutationforToken() throws IOException {
	        // Read a graphql file as an input stream
	        File file = new File("src/test/resources/Mutationfortoken.graphql");

		/*
		 * // Create a variables to pass to the graphql query 
		 * ObjectNode variables = new ObjectMapper().createObjectNode(); 
		 * variables.put("name", "Pikachu");
		 */

	        // Now parse the graphql file to a request payload string
	        String graphqlPayload = GraphqlTemplate.parseGraphqlwithoutvariables(file);

	        // Build and trigger the request
	        Response response = prepareResponseforauthtoken(graphqlPayload);

	        Assert.assertEquals(response.code(), 200, "Response Code Assertion");

	        String jsonData = response.body().string();
	        System.out.println(jsonData);
	      JsonNode jsonNode = new ObjectMapper().readTree(jsonData);
	      String AcessToken = jsonNode.get("data").get("login").get("accessToken").asText();
	  //    System.out.println(AcessToken);
	      return AcessToken;
	      
	    }

		
	   
	/*
	 * @Override public String getTestName() { // TODO Auto-generated method stub
	 * return null; }
	 */
	
}
