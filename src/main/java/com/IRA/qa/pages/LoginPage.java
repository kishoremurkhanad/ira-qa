package com.IRA.qa.pages;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.IRA.qa.base.TestBase;
import com.IRA.qa.util.PropertyManager;
import com.IRA.qa.util.WaitHelper;

public class LoginPage extends TestBase{
	
	
	//Page Factory - OR:
		//Signin Email field
	
	WebElement Login_email =  driver.findElement(By.xpath(Locatorproperties.getProperty("Login_email")));
  
	//Signin password field
	
	  WebElement Login_password = driver.findElement(By.xpath(Locatorproperties.getProperty("Login_password")));
	  
	//Signin button
	  
	WebElement SignInbutton = driver.findElement(By.xpath(Locatorproperties.getProperty("Login_loginbutton")));
	
	
		
	//Initializing the Page Objects:
	
	
	//Actions:
	public String validateLoginPageTitle(){
		//Login title
		  
			
		WebElement Login_Title = driver.findElement(By.xpath(Locatorproperties.getProperty("Profile_Display")));
		WaitHelper.waitForElement(driver,10000,Login_Title);
		String title = Login_Title.getText();
		System.out.println(title);
		return title ;
	}
	
	
	
	public void login(String username, String password){
		Login_email.sendKeys(username);
		Login_password.sendKeys(password);
		//loginBtn.click();
		SignInbutton.click();
		
		}
	
}
