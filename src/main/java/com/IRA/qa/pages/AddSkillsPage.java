package com.IRA.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.IRA.qa.base.TestBase;

public class AddSkillsPage extends TestBase{
	
	//Page Factory - OR:
	
		//Skillmap leftpane button
		
		WebElement Skillmap_leftpane_button =  driver.findElement(By.xpath(Locatorproperties.getProperty("Skillmap_leftpane_button")));

		//Skillmap dropdown
		
		WebElement Skillmap_dropdown =  driver.findElement(By.xpath(Locatorproperties.getProperty("Skillmap_dropdown")));
		
		//Skillmap Add Skill button
		
		WebElement Skillmap_AddSkill_button =  driver.findElement(By.xpath(Locatorproperties.getProperty("Skillmap_AddSkill_button")));
		
		//Skillmap Add Skill popup dialog
		
		WebElement Skillmap_AddSkill_popup_dialog =  driver.findElement(By.xpath(Locatorproperties.getProperty("Skillmap_AddSkill_popup_dialog")));
		
		//Skillmap Add Skill popup dialog Title
		WebElement Skillmap_AddSkill_popup_dialog_Title = driver.findElement(By.xpath(Locatorproperties.getProperty("Skillmap_AddSkill_popup_dialog_Title")));
	
		//Skillmap Add Skill popup dialog Skillname

		WebElement	Skillmap_AddSkill_popup_dialog_Skillname = driver.findElement(By.xpath(Locatorproperties.getProperty("Skillmap_AddSkill_popup_dialog_Skillname")));
			
		
		//Skillmap Add Skill popup dialog Cancel button
		
		WebElement	Skillmap_AddSkill_popup_dialog_Cancelbutton = driver.findElement(By.xpath(Locatorproperties.getProperty("Skillmap_AddSkill_popup_dialog_Cancelbutton")));
		
		//Skillmap Add Skill popup dialog Add button
		
		WebElement	Skillmap_AddSkill_popup_dialog_Addbutton  = driver.findElement(By.xpath(Locatorproperties.getProperty("Skillmap_AddSkill_popup_dialog_Addbutton")));
		
}
