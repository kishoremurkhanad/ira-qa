import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Questionbank {
	static WebDriver driver;
	static String baseurl;
	
	
	static By Login_email = By.xpath("(//input[@class='MuiInputBase-input MuiFilledInput-input'])[1]");
    static By Login_password  =  By.xpath("//input[@type='password']");
    static By Login_loginbutton  = By.xpath("//button[@type='submit']");
    static By Courses_Createcourse_button  = By.xpath("//span[text()='Create Course']");
    static By Questiobank_sidebutton  = By.xpath("//p[text()='Question Bank']");
    static By Questiobank_CreateQuestion_button  = By.xpath("//span[text()='Create question']");
    static By Questiobank_CreateQuestion_MCQbutton  = By.xpath("//div[text()='MCQ']");
    static By Questiobank_CreateQuestion_blankspace  = By.xpath("//div[@id='root']/div[1]/main[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[1]");
    
	@BeforeTest
	public void launch() {

        // Telling the system where to find the chrome driver
        System.setProperty(
                "webdriver.chrome.driver",
                "D:/browser drivers/chromedriver.exe");
        
        // Open the Chrome browser
        driver = new ChromeDriver();

   baseurl = "http://test.kh3ira.com/identity/login";
        
        try {
        	
        	// Open IRApage
            driver.get(baseurl);
        }
       
catch (final Exception e) {
    System.out.println(e.getClass().toString());
}
        }
	
	/**
	 * @throws InterruptedException 
	 * 
	 */
	@Test
	public void flow() throws InterruptedException {
		
		 driver.manage().timeouts().implicitlyWait(130, TimeUnit.SECONDS);
	     // Maximize the browser window
	        driver.manage().window().maximize();
	        WebDriverWait homepage_wait = new WebDriverWait(driver, 25);
	        homepage_wait.until(ExpectedConditions.visibilityOfElementLocated(Login_email));
	        driver.findElement(Login_email).sendKeys("aditya.prasad@knowledgehut.co");
	        driver.findElement(Login_password).sendKeys("T3st@1234");
	        driver.findElement(Login_loginbutton).click();
	        WebDriverWait Createcourse_wait = new WebDriverWait(driver, 25);
	        Createcourse_wait.until(ExpectedConditions.visibilityOfElementLocated(Courses_Createcourse_button));
	        driver.findElement(Questiobank_sidebutton).click();
	        WebDriverWait CreateQuestion_wait = new WebDriverWait(driver, 25);
	        CreateQuestion_wait.until(ExpectedConditions.visibilityOfElementLocated(Questiobank_CreateQuestion_button));
	        driver.findElement(Questiobank_CreateQuestion_button).click();
	        WebDriverWait CreateQuestion_results_wait = new WebDriverWait(driver, 25);
	        CreateQuestion_results_wait.until(ExpectedConditions.visibilityOfElementLocated(Questiobank_CreateQuestion_MCQbutton));
	        
	        
	        
	        WebElement source = driver.findElement(By.xpath("//div[text()='MMCQ']"));
	      WebElement target = driver.findElement(By.xpath("(((//div[@class='MuiGrid-root MuiGrid-item MuiGrid-grid-xs-true'])[2])/div/div/div/p)"));
	      String Str = target.getText();
	      System.out.println(Str);
		
		/*
		 * int x=source.getLocation().getX(); int y=source.getLocation().getY(); int
		 * x1=target.getLocation().getX(); int y1=target.getLocation().getY();
		 * System.out.println(x); System.out.println(y); System.out.println(x1);
		 * System.out.println(y1);
		 */
		 
	        Actions MCQdraganddrop = new Actions(driver);
	       
		/*
		 * MCQdraganddrop.keyDown(Keys.CONTROL) .click(source) .click(target)
		 * .keyUp(Keys.CONTROL);
		 * 
		 * Action selectMultiple = MCQdraganddrop.build();
		 * 
		 * // And execute it: selectMultiple.perform();
		 */
	     
	        		//Actions builder = new Actions(driver);
		
		/*
		 * Action MCQclickandhold = MCQdraganddrop.clickAndHold(source).build();
		 * 
		 * MCQclickandhold.perform(); Thread.sleep(2000); Action MCQmovetoblank =
		 * MCQdraganddrop.clickAndHold(source).moveToElement(target).release(source).
		 * build(); MCQmovetoblank.perform();
		 */
		/*
		 * Actions actions = new Actions(driver); actions.moveToElement(target);
		 */
	 
	    //    .moveToElement(second_table_row)
	   //     .release(table_data).build();

	      //  dragAndDrop.perform();
	        
	       // action.moveToElement(source).perform();
//driver.findElement(Questiobank_CreateQuestion_MCQbutton).click();
//action.dragAndDropBy(source, 450,0).build().perform();
//	      action.dragAndDropBy(source, 250,300).build().perform();
	      
	      
	      System.out.println("drag done");
	}

}
